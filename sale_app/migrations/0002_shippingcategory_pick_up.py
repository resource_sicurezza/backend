# Generated by Django 2.0.2 on 2018-03-12 03:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sale_app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='shippingcategory',
            name='pick_up',
            field=models.BooleanField(default=False),
        ),
    ]
