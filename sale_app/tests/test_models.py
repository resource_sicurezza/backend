from django.test import TestCase
import datetime
from model_mommy import mommy
from model_mommy.recipe import Recipe, foreign_key
from sale_app.models import Sale, SaleProduct, DeliveryOrder, ShopCar
from stock_app.models import Stock, StockDetails
from products_app.models import ProductAttribute, AttributeValue, Product
from sale_app.constants import PENDING


class Core(TestCase):
    def setUp(self):
        self.user = mommy.make('client_app.CustomUser')
        shipping_categoy = mommy.make('sale_app.ShippingCategory')
        self.products_to_sell = {
            'data':[
                {  'idProduct': 1, 'variations': [ { 'id':1, 'quantity':209, 'new_to_sell':81 } ] },
                {  'idProduct': 2, 'variations': [ { 'id':2, 'quantity':110, 'new_to_sell':45 } ] },
                {  'idProduct': 3, 'variations': [ { 'id':3, 'quantity':3, 'new_to_sell':3 } ] },
                {  'idProduct': 4, 'variations': [ { 'id':4, 'quantity':10, 'new_to_sell':3 } ] }
            ]
        }
        self.products = [
            {'id':1, 'price':90, 'dolar_price':33, 'sku':'pr-1', 'in_stock':True,
             'total_stock':400, 'total_stock_to_sell':220, 'to_sell':220, 'to_sell_limit':80,
             'transfer_quantity':10, 'reserved':300, 'reserved_limit':25},
            {'id':2, 'price':60, 'dolar_price':20, 'sku':'pr-2', 'in_stock':True,
             'total_stock':100, 'total_stock_to_sell':120, 'to_sell':120, 'to_sell_limit':80,
             'transfer_quantity':30, 'reserved':35, 'reserved_limit':25},
            {'id':3, 'price':60, 'dolar_price':20, 'sku':'pr-3', 'in_stock':True,
             'total_stock':100, 'total_stock_to_sell':120, 'to_sell':6, 'to_sell_limit':10,
             'transfer_quantity':0, 'reserved':0, 'reserved_limit':0},
            {'id':4, 'price':60, 'dolar_price':20, 'sku':'pr-4', 'in_stock':True,
             'total_stock':40, 'total_stock_to_sell':10, 'to_sell':3, 'to_sell_limit':1,
             'transfer_quantity':0, 'reserved':0, 'reserved_limit':0},
        ]
        category = mommy.make('products_app.Category')
        tag = mommy.make('products_app.Tag')
        attribute = mommy.make('products_app.Attribute')
        attribute.name = 'color'
        attribute.save()
        for pr in self.products:
            product = Product(name="product-"+str(pr['id']), price=pr['price'], 
                            dolar_price=pr['dolar_price'], sku=pr['sku'],
                            in_stock=pr['in_stock'])
            product.save()
            product.category.add(category)
            product.tag.add(tag)
            product.save()
            product_attr = ProductAttribute(attribute=attribute, product=product)
            product_attr.save()
            product_attr_value = AttributeValue(product_attribute=product_attr,
                                                value='RED', price_variation=30)
            product_attr_value.save()
            stock = Stock(product=product_attr_value, total_stock=pr['total_stock'], 
                          transfer_quantity=pr['transfer_quantity'], reserved=pr['reserved'], 
                          reserved_limit=pr['reserved_limit'], notes='notes')
            stock.save()
            stock_detail = StockDetails(stock=stock, total_stock_to_sell=pr['total_stock_to_sell'],
                            to_sell=pr['to_sell'], to_sell_limit=pr['to_sell_limit'], visible=True,
                            date_begin=datetime.datetime(2018,2,1), date_end=datetime.datetime(2018,2,27))
            stock_detail.save()

class SaleTest(Core):
    """
        CREACION DE PRODUCTOS | ATRIBUTOS | VARIACIONES | STOCK Y DETALLE DE STOCK

        5 productos
            --> Variaciones de color / RED - variación del precio 30 soles
                ------STOCK--------
                --> Total stock: 400
                --> Reserva: 80
                --> Limite de reserva: 10
                --> Cantidad de transferencia: 90
                ------STOCK DETAILS----
                --> Stock total para vender:220
                --> Stock límite: 80
        """

    def setUp(self):
        super(SaleTest, self).setUp()
        self.sell()

    def sell(self):
        self.sale = Sale(state=PENDING, date_sale=datetime.datetime(2018,2,26),
                    amount_sale=200, user=self.user, detail='Detalle')
        self.sale.save()
        for pr_sell in self.products_to_sell['data']:
            for pr in pr_sell['variations']:
                go_sale = True
                product = Product.objects.get(pk=pr_sell['idProduct'])
                pdct_attr_value = AttributeValue.objects.get(pk=pr['id'])
                details_stock = pdct_attr_value.stock.details_stock
                if details_stock.to_sell+pdct_attr_value.stock.reserved >= pr['quantity']:
                    details_stock.to_sell -= pr['quantity']
                    if pdct_attr_value.stock.reserved!=0:
                        while details_stock.to_sell <= details_stock.to_sell_limit:
                            if pdct_attr_value.stock.transfer_quantity > pdct_attr_value.stock.reserved:
                                details_stock.to_sell += pdct_attr_value.stock.reserved
                                pdct_attr_value.stock.reserved = 0
                                details_stock.save()
                                pdct_attr_value.save()
                                print('NOTIFICACIÓN: SE ACABO LA RESERVA')
                                if details_stock.to_sell <= details_stock.to_sell_limit:
                                    print('NOTIFICACIÓN: SE LLEGO AL LIMITE DE VENTAS')
                                break
                            else:
                                ###TRANSFIRIENDO###
                                remaining = ( pdct_attr_value.stock.reserved - 
                                                pdct_attr_value.stock.transfer_quantity )
                                pdct_attr_value.stock.reserved = remaining
                                details_stock.to_sell += pdct_attr_value.stock.transfer_quantity
                                details_stock.save()
                                pdct_attr_value.save()
                                if remaining >= pdct_attr_value.stock.reserved_limit:
                                    print("NOTIFICACIÓN: SE PASO EL LIMITE DE LA RESERVA")
                    if go_sale and details_stock.to_sell>=0:
                        sale_product = SaleProduct(attribute_product_value=pdct_attr_value, 
                                                    sale=self.sale,
                                                    description_product=product.name,
                                                    image_product=product.catalog_image,
                                                    price_product=product.price,
                                                    quantity=pr['quantity'])
                        sale_product.save()
                        sshipping_zone = mommy.make('sale_app.ShippingZone')
                        delivery_order = DeliveryOrder(shipping_zone=sshipping_zone, sale=self.sale, 
                                    address='Av Pargo y Aliaga 515')
                        delivery_order.save()
                        details_stock.save()
                    else:
                        print("NOTIFICACIÓN: NO COMPRAR")
    
    def test_sell_with_transference(self):
        product = Product.objects.get(pk=1)
        product_attr_value = AttributeValue.objects.get(pk=product.pk)
        details_stock = product_attr_value.stock.details_stock
        new_to_sell = self.products_to_sell['data'][0]['variations'][0]['new_to_sell']
        self.assertEqual(details_stock.to_sell, new_to_sell)

    def test_sell_with_transference_and_limit_break(self):
        product = Product.objects.get(pk=2)
        product_attr_value = AttributeValue.objects.get(pk=product.pk)
        details_stock = product_attr_value.stock.details_stock
        new_to_sell = self.products_to_sell['data'][1]['variations'][0]['new_to_sell']
        self.assertEqual(details_stock.to_sell, new_to_sell)

    def test_sell_simple(self):
        product = Product.objects.get(pk=3)
        product_attr_value = AttributeValue.objects.get(pk=product.pk)
        details_stock = product_attr_value.stock.details_stock
        new_to_sell = self.products_to_sell['data'][2]['variations'][0]['new_to_sell']
        self.assertEqual(details_stock.to_sell, new_to_sell)

    def test_sell_failed(self):
        product = Product.objects.get(pk=4)
        product_attr_value = AttributeValue.objects.get(pk=product.pk)
        details_stock = product_attr_value.stock.details_stock
        new_to_sell = self.products_to_sell['data'][3]['variations'][0]['new_to_sell']
        self.assertEqual(details_stock.to_sell, new_to_sell)

    def test_sell(self):
        sale = Sale.objects.get(pk=1)
        print(sale)
        self.assertEqual(False, False)

