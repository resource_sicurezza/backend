from django.db import models
from products_app.models import AttributeValue, Product
from client_app.models import CustomUser
from .mixins import SellMixin
from .constants import *


class Shipping(models.Model):
    """
    Attributes
    name:
        Nombre del local
    from_address:
        Dirección del almacen
    """
    name = models.CharField(max_length=100)
    from_address = models.CharField(max_length=300)

    def __str__(self):
        return '{0} - {1}'.format(self.name, self.from_address)
    

class ShippingCategory(models.Model):
    """
    Attributes
    category:
        Categoría del envío - Doméstico / Extranjero
    shipping:
        Objeto envío
    """
    category = models.CharField(max_length=2, choices=SHIPPING_ZONE_CATEGORY)
    shipping = models.ForeignKey(Shipping, on_delete=models.CASCADE, 
                                related_name='shipping_category')

    def __str__(self):
        return '{0} - {1}'.format(self.category, self.shipping)


class ShippingZone(models.Model):
    """
    Attributes
    name:
        Nombre del tipo de zona, ejemp: Envío express-Recojo en tienda
    shipping_category:
        Objeto ShippingCategory
    min_prcie:
        El precio mínimo para aplicar al tipo de zona
    max_price:
        Precio máximo para aplicaar al tipo de zona
    to_up:
        Del precio mínimo para adelante
    rate_amount:
        Costo asociado al tipo de zona
    """
    name = models.CharField(max_length=250)
    shipping_category = models.ForeignKey(ShippingCategory, on_delete=models.CASCADE,
                                         related_name='shipping_zone')
    min_price = models.DecimalField(max_digits=6, decimal_places=2, null=True, default=0)
    max_price = models.DecimalField(max_digits=6, decimal_places=2, null=True, default=0)
    to_up = models.BooleanField(default=False) 
    rate_amount = models.DecimalField(max_digits=6, decimal_places=2, null=True, default=0)

    def __str__(self):
        return '{0} - {1}'.format(self.rate_amount, self.name)


class Sale(SellMixin, models.Model):
    """
    Attribute
    state: 
        Pendiente / Pagado
    product_attribute_value: 
        Productos/Variación - Pueden ser varios por venta
    date_sale: 

    """
    state = models.CharField(max_length=2, choices=STATE_CHECKOUT)
    product_attribute_value = models.ManyToManyField(AttributeValue, related_name='sale',
                                                    through='SaleProduct')
    date_sale = models.DateField(auto_now=True)
    amount_sale = models.DecimalField(max_digits=10, decimal_places=2)
    email = models.CharField(max_length=30)
    phone = models.CharField(max_length=12)
    address = models.CharField(max_length=130, null=True, blank=True)
    user = models.ForeignKey(CustomUser, on_delete=models.PROTECT, null=True, blank=True)
    detail = models.CharField(max_length=300)
    token = models.CharField(max_length=40, blank=True, null=True)
    


class DeliveryOrder(models.Model):
    """
    Attribute
    date:
        Fecha de delivery
    shipping_zone:
        Zona de envío
    sale:
        Venta asociado
    address:
        Dirección del cliente
    """
    date = models.DateField(auto_now_add=True)
    shipping_zone = models.ForeignKey(ShippingZone, on_delete=models.CASCADE,
                                        related_name='delivery_order')
    sale = models.ForeignKey(Sale, on_delete=models.CASCADE)
    address = models.CharField(max_length=300)


class SaleProduct(models.Model):
    attribute_product_value = models.ForeignKey(AttributeValue, on_delete=models.PROTECT,
                                                related_name='sale_product')
    sale = models.ForeignKey(Sale, on_delete=models.PROTECT, related_name='sale_product')
    description_product = models.CharField(max_length=300)
    image_product = models.CharField(max_length=200)
    price_product = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.IntegerField()

    def __str__(self):
        return str(self.attribute_product_value)


class ShopCar(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='shop_car')
    anonymous = models.BooleanField(default=True)
    user = models.OneToOneField(CustomUser, on_delete=models.PROTECT, null=True, blank=True, 
                                related_name='shop_car')
    anonymous_token = models.AutoField(primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    quantity = models.IntegerField(default=1)

    def __str__(self):
        return '{0} Anonymous:{1}'.format(self.product, self.anonymous)




