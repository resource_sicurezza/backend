from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter
from sale_app.views import SaleCrudView


router = DefaultRouter()
router.register(r'sale', SaleCrudView)

urlpatterns = [
    url(r'^', include(router.urls))
]