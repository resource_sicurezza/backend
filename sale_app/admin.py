from django.contrib import admin
from sale_app.models import (DeliveryOrder, Sale, SaleProduct, Shipping,
                             ShippingCategory, ShippingZone, ShopCar)

admin.site.register(DeliveryOrder)
admin.site.register(Sale)
admin.site.register(SaleProduct)
admin.site.register(Shipping)
admin.site.register(ShippingCategory)
admin.site.register(ShippingZone)
admin.site.register(ShopCar)