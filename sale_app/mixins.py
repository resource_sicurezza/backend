from django.db import models
from .constants import PAID, PENDING

class SellMixin(models.Model):
    """
    Mixin para realizar la venta
    :model:`sale_app.Sale`.

    sell:
        state: PAID/PENDING
        attrvalueproduct: [ap1, ap2, ap3, ap4]
        product: <product>
        date_sale: 2018-04-20
        amount_sale: Total de los productos
        user: <user>
        detail: Detalle de la venta
    """
    class Meta:
        abstract = True

    def pre_sell(self, state, products, attrvalueproduct, date_sale, amount_sale, user, detail):
        sale = Sale(state=PENDING, date_sale=datetime.datetime.now(),
                    amount_sale=amount_sale, user=user, detail=detail)

    def pre_sell_test(self):
        print("Example")

    
    def sell(self, pk):
        sale = Sale.object.get(pk=pk)
        sale.state = PAID
        sale.save()
