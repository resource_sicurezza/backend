from rest_framework import serializers
from sale_app.models import Sale, SaleProduct
from client_app.models import CustomUser
from rest_framework.authtoken.models import Token
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist


class SaleProductSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = SaleProduct
		fields = '__all__'



class SaleSerializers(serializers.ModelSerializer):
	product = SaleProductSerializer(source='sale_product', many=True)

	class Meta:
		model = Sale
		fields = ('state', 'date_sale', 'amount_sale', 'email', 'phone', 'address', 'detail',
				  'token', 'product')

	def create(self, validated_data):
		if validated_data['token']:
			try:
				user = CustomUser.objects.get(auth_token=validated_data.pop('token'))
				validated_data['user'] = user
				sale = Sale.objects.create(**validated_data)
				return sale
			except ObjectDoesNotExist:
				return JsonResponse({'token': "user not found", 'logged': False })
		else:
			validated_data.pop('token')
			sale = Sale.objects.create(**validated_data)
			return sale