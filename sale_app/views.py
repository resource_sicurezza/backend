from rest_framework import viewsets
from sale_app.serializers import SaleSerializers
from sale_app.models import Sale


class SaleCrudView(viewsets.ModelViewSet):
	queryset = Sale.objects.all()
	serializer_class = SaleSerializers