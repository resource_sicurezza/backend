from django.test import TestCase
from rest_framework.authtoken.models import Token
from client_app.models import CustomUser


class ProfileTest(TestCase):
    
    def setUp(self):
        #self.factory = RequestFactory()
        self.user = CustomUser.objects.create_user(email='jacob@test.com', date_of_birth='1993-04-20', password='top_secret')

    def test_profile_signal(self):
        self.assertEqual(str(self.user.profile_user), 'jacob@test.com')

    def test_generate_token(self):
        token_value = Token.objects.filter(user=self.user)
        self.assertEqual(token_value.exists(), True)
