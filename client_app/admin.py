from django.contrib import admin
from .models import Profile, CreditCard, Campaign, Public
import client_app.forms

admin.site.register(Profile)
admin.site.register(CreditCard)
admin.site.register(Campaign)
admin.site.register(Public)
