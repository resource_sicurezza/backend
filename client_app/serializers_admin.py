from rest_framework import serializers
from .models import CustomUser


class ListClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomUser
        fields = ('name', 'last_name', 'email', 'phone')
