from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)


class CustomUserManager(BaseUserManager):
    def create_user(self, email, date_of_birth, password=None):
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(
            email=self.normalize_email(email),
            date_of_birth=date_of_birth
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, date_of_birth=None):
        user = self.create_user(
            email,
            date_of_birth,
            password=password
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser):
    MAS = 'Masculino'
    FEM = 'Femenino'
    OTH = 'Other'
    GENDER_CHOICES = ((MAS, 'Masculino'),(FEM, 'Femenino'),(OTH,'Otros'))
    
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    username = models.CharField(max_length=90)
    name = models.CharField(max_length=90, null=False, blank=True)
    last_name = models.CharField(max_length=100, null=False, blank=True)
    gender = models.CharField(max_length=9, choices=GENDER_CHOICES, default=FEM)
    phone = models.CharField(max_length=20, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    #REQUIRED_FIELDS = ['date_of_birth']


    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

class Profile(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, related_name='profile_user')
    phone = models.CharField(max_length=20, blank=True)
    phone_optional = models.CharField(max_length=15, blank=True, null=True)
    state = models.CharField(max_length=30, blank=True)
    province = models.CharField(max_length=30, blank=True)
    district = models.CharField(max_length=30, blank=True)
    address = models.CharField(max_length=30, blank=True)
    nro_lote = models.CharField(max_length=30, blank=True)
    department_nro = models.CharField(max_length=30, blank=True)
    urb = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return self.user.email


class CreditCard(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, related_name='credit_card_user')
    user_name = models.CharField(max_length=150, null=False, blank=True)
    user_last_name = models.CharField(max_length=150, null=False, blank=True)
    phone = models.CharField(max_length=15, null=False, blank=True)
    address = models.CharField(max_length=200, null=False, blank=True)
    number = models.CharField(max_length=16, null=False, blank=True)
    date = models.CharField(max_length=7, null=False, blank=True)
    cvv = models.CharField(max_length=3, null=False, blank=True)
    type_card = models.CharField(max_length=20, null=False, blank=True)


class Public(models.Model):
    name = models.CharField(max_length=70)
    description = models.TextField(max_length=200)
    users = models.ManyToManyField(CustomUser, default='', related_name='public_users')

    def __str__(self):
        return self.name


class Campaign(models.Model):
    name = models.CharField(max_length=70, null=False)
    public = models.ManyToManyField(Public, related_name='campaign_public')
    date_start = models.DateField( auto_now=False, auto_now_add=False)
    date_end = models.DateField( auto_now=False, auto_now_add=False)
    api_mailchimp = models.CharField(max_length=100)

    def __str__(self):
        return self.name
