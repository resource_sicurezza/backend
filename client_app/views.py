from .serializers import (ProfileSerializer, CreditCardSerializer, 
                        PublicSerializer, CampaignSerializer, UserSerializer, LoginSerializer)
from .models import Profile, CreditCard, Public, Campaign, CustomUser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .permissions import AllowAnyGetPermission
from rest_framework.authtoken.models import Token
from rest_framework import viewsets
from django.contrib.auth import authenticate
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


class ProfileCrudView(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = (AllowAnyGetPermission,)


class CreditCardCrudView(viewsets.ModelViewSet):
    queryset = CreditCard.objects.all()
    serializer_class = CreditCardSerializer
    permission_classes = (AllowAnyGetPermission,)


class PublicCrudView(viewsets.ModelViewSet):
    queryset = Public.objects.all()
    serializer_class = PublicSerializer
    permission_classes = (AllowAnyGetPermission,)


class CampaignCrudView(viewsets.ModelViewSet):
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer
    permission_classes = (AllowAnyGetPermission,)


class UserCrudView(viewsets.ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAnyGetPermission,)

    def update(self, request, pk):
        print("request")
        print(request.data)
        return super(UserCrudView, self).update(request, pk)

    def list(self, request, *args, **kwargs):
        key = request.META['HTTP_AUTHORIZATION']
        user = Token.objects.get(key=key).user
        serializer = UserSerializer(user)
        return JsonResponse(serializer.data)


class Login(APIView):

    def post(self, request, format=None):
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            user = authenticate(username=serializer.data['user'], password=serializer.data['password'])
            if user:
                token = Token.objects.get(user=user)
                return Response({'token': token.key, 'user': user.id, 'login': True}, status=status.HTTP_201_CREATED)
            else:
                    return Response({'login': False}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)