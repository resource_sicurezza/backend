from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from .models import Profile, CustomUser, CreditCard


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


def save_user_profile(sender, instance, **kwargs):
    instance.profile_user.save()


def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
        CreditCard.objects.create(user=instance)

post_save.connect(create_user_profile, sender=CustomUser,
                  dispatch_uid="create_user_profile")
post_save.connect(save_user_profile, sender=CustomUser,
                  dispatch_uid="save_user_profile")
post_save.connect(create_auth_token, sender=CustomUser,
                  dispatch_uid="create_auth_token")