from django.apps import AppConfig
from django.db.models.signals import post_save


class ClientAppConfig(AppConfig):
    name = 'client_app'

    def ready(self):
        import client_app.signals