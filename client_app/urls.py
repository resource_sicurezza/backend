from client_app import views
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views as rest_framework_views
from client_app.views_admin import ListClientView


router = DefaultRouter()
router.register(r'profile', views.ProfileCrudView)
router.register(r'credit_card', views.CreditCardCrudView)
router.register(r'public', views.PublicCrudView)
router.register(r'campaign', views.CampaignCrudView)
router.register(r'user', views.UserCrudView)


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^login/$', views.Login.as_view(), name='login'),
    url(r'^list-user/', ListClientView.as_view()),
    url(r'^get_auth_token/$', rest_framework_views.obtain_auth_token, name='get_auth_token'),
]