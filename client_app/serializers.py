from rest_framework import serializers
from .models import Profile, CreditCard, Public, Campaign, CustomUser


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = '__all__'


class CreditCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = CreditCard
        fields = '__all__'


class PublicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Public
        fields = '__all__'


class CampaignSerializer(serializers.ModelSerializer):
    class Meta:
        model = Campaign
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    profile_user = ProfileSerializer(required=False)
    credit_card_user = CreditCardSerializer(required=False)
    
    class Meta:
        model = CustomUser
        fields = ('name', 'last_name', 'email', 'gender', 'phone', 'date_of_birth', 'profile_user', 'credit_card_user')


class LoginSerializer(serializers.Serializer):
    user = serializers.CharField(required=True, max_length=100)
    password = serializers.CharField(required=True, max_length=100)