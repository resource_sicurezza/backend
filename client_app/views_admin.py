from rest_framework import generics
from .serializers_admin import ListClientSerializer
from .models import CustomUser


class ListClientView(generics.ListAPIView):
    serializer_class = ListClientSerializer
    queryset = CustomUser.objects.filter(is_admin=False)
    #permission_classes = (IsAuthenticatedPermission, )