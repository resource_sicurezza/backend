from django_filters import rest_framework as filters
from .models import Product


class CatalogFilter(filters.FilterSet):
    min_price = filters.NumberFilter(name="price", lookup_expr='gte')
    max_price = filters.NumberFilter(name="price", lookup_expr='lte')
    color = filters.CharFilter(name="color", label='color', method='filter_color')
    in_sale = filters.CharFilter(name="sale", label='sale', method='filter_sale')

    def filter_color(self, queryset, name, value):
        colors = value.split(",")
        return queryset.filter(product_attribute__attribute_value__value__in=colors)

    def filter_sale(self, queryset, name, value):
        active = True
        if value == '1':
            active = False
        return queryset.filter(sale_price__isnull=active)
    
    class Meta:
        model = Product
        fields = ['category', 'tag', 'min_price', 'max_price', 'in_stock',
                'product_details__average_rate', 'color']