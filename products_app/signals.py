from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from .models import ReviewRate, ProductDetails, Discount


def add_review_rate(sender, instance, created, **kwargs):
    product_details = ProductDetails.objects.get(description=instance.product_detail)
    product_details.rate_counts += 1
    product_details.sum_rates += instance.rate
    average = product_details.sum_rates / ReviewRate.objects.count()
    product_details.average_rate = round(average, 1)
    product_details.save()


def delete_review_rate(sender, instance, created, **kwargs):
    product_details = ProductDetails.objects.get(description_product=instance.product_detail)
    sum_rate = product_details.sum_rates - instance.rate_product
    count = ReviewRate.objects.count() - 1
    product_details.average_rate = sum_rate / count
    product_details.rate_counts = count
    product_details.sum_rates = sum_rate
    product_details.save()


def apply_discount(sender, instance, created, **kwargs):
    #product_details = Discount.objects.get(pk=instance)
    print(instance)


post_save.connect(add_review_rate, sender=ReviewRate,
                  dispatch_uid="add_review_rate")
post_delete.connect(delete_review_rate, sender=ReviewRate,
                  dispatch_uid="delete_review_rate")
post_save.connect(apply_discount, sender=Discount,
                  dispatch_uid="apply_discount")