from products_app.views import CatalogView, DetailProductView
from products_app.views_admin import (ProductList, ProductCrud, AttributeCrud, TagCrud,
                                      CategoryCrud, GroupCrudView, LookBookCrudView, DeleteProducts,
                                      DeleteCategories, DeleteTags, DeleteAttributes, DeleteProductAttributes,
                                      ProductAttributeCrud, SmallProductCrud, AttibuteValueCRUD, DeleteGroups,
                                      DiscountCrudView, DeleteDiscounts, GetVariationsProducts, GetVariations)
from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter


catalog = CatalogView.as_view({
    'get': 'list'
})

router = DefaultRouter()
router.register(r'admin-group', GroupCrudView)
router.register(r'lookbook', LookBookCrudView)
router.register(r'admin-product', ProductCrud)
router.register(r'admin-attribute', AttributeCrud)
router.register(r'admin-category', CategoryCrud)
router.register(r'admin-tags', TagCrud)
router.register(r'admin-discount', DiscountCrudView)
router.register(r'admin-product-attribute', ProductAttributeCrud)

urlpatterns = [
    url(r'^catalog/', catalog, name='products-catalog'),
    url(r'^varition-product/(?P<pk>\d+)/$', GetVariationsProducts.as_view(), name='varition-product'),
    url(r'^variations/', GetVariations.as_view()),
    url(r'^detail-product/(?P<pk>\d+)/$', DetailProductView.as_view(), name='detail-product'),
    url(r'^admin-list-products/', ProductList.as_view()),
    url(r'^admin-list-small-products/', SmallProductCrud.as_view()),
    url(r'^admin-attribute-value/', AttibuteValueCRUD.as_view()),
    url(r'^set-delete-categories/', DeleteCategories.as_view()),
    url(r'^set-delete-tags/', DeleteTags.as_view()),
    url(r'^set-delete-product-attributes/', DeleteProductAttributes.as_view()),
    url(r'^set-delete-attributes/', DeleteAttributes.as_view()),
    url(r'^set-delete-products/', DeleteProducts.as_view()),
    url(r'^set-delete-groups/', DeleteGroups.as_view()),
    url(r'^set-delete-discounts/', DeleteDiscounts.as_view()),
    url(r'^', include(router.urls)),
]
