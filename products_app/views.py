from rest_framework import generics
from products_app.serializers import CatalogSerializer, DetailProductSerializer
from products_app.models import Product
from .filters import CatalogFilter
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.pagination import LimitOffsetPagination


class DetailProductView(generics.RetrieveAPIView):
    queryset = Product.objects.all()
    serializer_class = DetailProductSerializer


class CatalogView(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = CatalogSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = CatalogFilter
    pagination_class = LimitOffsetPagination
