from django.db import models


class Tag(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.name


class Attribute(models.Model):
    COLOR = 'CL'
    OTHER = 'OT'
    ATTRIBUTE_TYPE_CHOICES = ((COLOR, 'COLOR'), (OTHER, 'OTHER'))
    type = models.CharField(max_length=2, choices=ATTRIBUTE_TYPE_CHOICES)
    name = models.CharField(max_length=40, null=True)

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    product_category = models.ForeignKey(Category, on_delete=models.CASCADE, default='',
                                         related_name='sub_categories', null=True, blank=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Color(models.Model):
    name = models.CharField(max_length=20, null=False)
    hex_value = models.CharField(max_length=10, default='#ffffff')

    def __str__(self):
        return '{0} - {1}'.format(self.name, self.hex_value)


class Product(models.Model):
    TOP = 'TP'
    MIDDLE = 'MD'
    BOTTOM = 'BT'
    POSITION = ((TOP, 'TOP'), (MIDDLE, 'MD'), (BOTTOM, 'BOTTOM'))

    catalog_image = models.ImageField(null=True, blank=True)
    catalog_image_secondary = models.ImageField(null=True, blank=True)
    name = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    dolar_price = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    position = models.CharField(max_length=2, choices=POSITION, null=True, blank=True, default=TOP)
    sku = models.CharField(max_length=50, null=True, blank=True)
    in_stock = models.BooleanField()
    sale_price = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    amount_dsct = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    attribute = models.ManyToManyField(Attribute, related_name='product', null=True,
                                       blank=True, through='ProductAttribute')
    category = models.ManyToManyField(Category, default='', related_name='products', null=True, blank=True)
    tag = models.ManyToManyField(Tag, default='', related_name='products', null=True, blank=True)
    stock = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class ProductAttribute(models.Model):
    attribute = models.ForeignKey(Attribute, on_delete=models.CASCADE,
                                  related_name='product_attribute', verbose_name='Atributo')
    product = models.ForeignKey(Product, on_delete=models.CASCADE,
                                related_name='product_attribute', blank=True, verbose_name='Producto')

    class Meta:
        unique_together = ('attribute', 'product',)

    @staticmethod
    def add_child(values):
        AttributeValue.objects.create(**values)

    def __str__(self):
        return self.product.name + ' - ' + self.attribute.name

    @staticmethod
    def get_name_related_field():
        return 'product_attribute'


class SKU(models.Model):
    sku = models.CharField(max_length=100, default='sku-')
    stock = models.IntegerField(default=0)

    def __str__(self):
        return '{0} '.format(self.sku)


class AttributeValue(models.Model):
    product_attribute = models.ForeignKey(ProductAttribute, on_delete=models.CASCADE,
                                          related_name='attribute_value')
    value = models.CharField(max_length=100, null=True, blank=True)
    sku = models.ForeignKey(SKU, null=True, blank=True, related_name='attribute_values', on_delete=models.PROTECT)

    def __str__(self):
        return '{0} - {1} - {2}'.format(self.product_attribute.product.name,
                                                    self.product_attribute, self.value)


class Promotions(models.Model):
    name = models.CharField(max_length=255, null=True)
    date_begin = models.DateField()
    date_end = models.DateField()
    main_product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='promotion')
    product_promotion = models.ManyToManyField(Product, related_name='product_promotion',
                                               through='ProductPromotions')

    def __str__(self):
        return self.name_promotion


class ProductPromotions(models.Model):
    promotion = models.ForeignKey(Promotions, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    price_product = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return self.promotion.name


class ProductDetails(models.Model):
    product = models.OneToOneField(Product, on_delete=models.CASCADE, default='',
                                   related_name='product_details', null=True, blank=True)
    description = models.CharField(max_length=500, blank=True)
    rich_snippet_description = models.CharField(max_length=500, blank=True)
    keywords = models.CharField(max_length=100, blank=True)
    average_rate = models.DecimalField(max_digits=3, decimal_places=1, null=True, default=0, blank=True)
    rate_counts = models.DecimalField(max_digits=3, decimal_places=1, null=True, default=0)
    sum_rates = models.DecimalField(max_digits=3, decimal_places=1, null=True, default=0)

    def __str__(self):
        return self.description


class ReviewRate(models.Model):
    product_detail = models.ForeignKey(ProductDetails, on_delete=models.CASCADE,
                                       related_name='review_rate')
    review = models.CharField(max_length=100, null=True)
    rate = models.IntegerField(null=True)


class Group(models.Model):
    product = models.ManyToManyField(Product, default='', related_name='group_product', blank=True)
    name = models.CharField(max_length=150, null=True)
    description_group = models.CharField(max_length=150)

    def __str__(self):
        return self.name


class Discount(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, default='',
                                related_name='discount_product', null=True, blank=True)
    group = models.ForeignKey(Group, default='', related_name='discount_group',
                              null=True, on_delete=models.CASCADE, blank=True)
    date_begin = models.DateField(null=True)
    date_end = models.DateField(null=True)
    state = models.BooleanField()
    description = models.CharField(max_length=255, null=True)
    percentage = models.DecimalField(max_digits=6, decimal_places=2, null=True)

    def __str__(self):
        return '{0} - {1}  {2}'.format(self.product, self.group, str(self.percentage))


class LookBook(models.Model):
    name = models.CharField(max_length=40)
    products = models.ManyToManyField(Product)

    def __str__(self):
        return self.name


class Image(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, default='',
                                related_name='image', null=True, blank=True)
    path = models.ImageField(upload_to="media/", null=True, blank=True)
    position = models.IntegerField(null=True)

    def __str__(self):
        return self.product.name
