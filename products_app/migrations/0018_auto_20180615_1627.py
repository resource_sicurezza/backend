# Generated by Django 2.0.2 on 2018-06-15 21:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products_app', '0017_auto_20180615_1613'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productdetails',
            name='average_rate',
            field=models.DecimalField(blank=True, decimal_places=1, default=0, max_digits=3, null=True),
        ),
    ]
