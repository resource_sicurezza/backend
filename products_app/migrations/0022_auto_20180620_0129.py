# Generated by Django 2.0.2 on 2018-06-20 06:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products_app', '0021_auto_20180619_1442'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discount',
            name='group',
            field=models.ForeignKey(blank=True, default='', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='discount_group', to='products_app.Group'),
        ),
        migrations.AlterField(
            model_name='discount',
            name='product',
            field=models.ForeignKey(blank=True, default='', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='discount_product', to='products_app.Product'),
        ),
    ]
