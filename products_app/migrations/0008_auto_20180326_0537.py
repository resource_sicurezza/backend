# Generated by Django 2.0.2 on 2018-03-26 05:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products_app', '0007_productattribute_is_color'),
    ]

    operations = [
        migrations.CreateModel(
            name='Color',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
                ('hex_value', models.CharField(default='#ffffff', max_length=10)),
            ],
        ),
        migrations.AlterField(
            model_name='attributevalue',
            name='value',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='attributevalue',
            name='value_color',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='attribute_value', to='products_app.Color'),
        ),
    ]
