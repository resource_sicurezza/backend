from django.contrib import admin
from products_app.models import (Product, ProductDetails, Discount, Tag, Category,
                                 SubCategory, Image, Attribute, AttributeValue,
                                 ReviewRate, ProductPromotions, ProductAttribute,
                                 Color, LookBook, SKU)

admin.site.register(Product)
admin.site.register(ProductAttribute)
admin.site.register(ProductDetails)
admin.site.register(LookBook)
admin.site.register(Discount)
admin.site.register(Tag)
admin.site.register(Category)
admin.site.register(SubCategory)
admin.site.register(Image)
admin.site.register(Attribute)
admin.site.register(AttributeValue)
admin.site.register(ReviewRate)
admin.site.register(ProductPromotions)
admin.site.register(Color)
admin.site.register(SKU)
