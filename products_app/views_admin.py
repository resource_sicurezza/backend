from rest_framework.response import Response
from client_app.permissions import AllowAnyGetPermission
from rest_framework import viewsets, generics
from .models import Product, Category, Attribute, Tag, Discount, LookBook, ProductAttribute, AttributeValue, Group
from .serializers_admin import (ProductListSerializer, ProductSerializer, SetProductSerializer,
                                AttributeSerializer, SetProductAttributeSerializer,
                                CategorySerializer, TagSerializer, AttributeValueCRUDSerializer, GroupSerializer,
                                LookBookSerializer, SetCategorySerializer, SmallProductSerializer,
                                SetTagSerializer, SetAttributeSerializer, ProductAttributeSerializer,
                                DiscountSerializer, SetDiscountSerializer)


class DiscountCrudView(viewsets.ModelViewSet):
    queryset = Discount.objects.all()
    serializer_class = DiscountSerializer


class GroupCrudView(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class LookBookCrudView(viewsets.ModelViewSet):
    queryset = LookBook.objects.all()
    serializer_class = LookBookSerializer


class ProductList(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductListSerializer
    permission_classes = (AllowAnyGetPermission,)


class ProductCrud(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    #permission_classes = (AllowAnyGetPermission,)


class SmallProductCrud(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = SmallProductSerializer


class CategoryCrud(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    #permission_classes = (AllowAnyGetPermission,)


class AttributeCrud(viewsets.ModelViewSet):
    queryset = Attribute.objects.all()
    serializer_class = AttributeSerializer
    #permission_classes = (AllowAnyGetPermission,)


class ProductAttributeCrud(viewsets.ModelViewSet):
    queryset = ProductAttribute.objects.all()
    serializer_class = ProductAttributeSerializer
    #permission_classes = (AllowAnyGetPermission,)


class TagCrud(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    #permission_classes = (AllowAnyGetPermission,)


class AttibuteValueCRUD(generics.CreateAPIView):
    queryset = AttributeValue.objects.all()
    serializer_class = AttributeValueCRUDSerializer

    def create(self, request, *args, **kwargs):
        for val in request.data['values']:
            pr_attr = ProductAttribute.objects.get(pk=val['product_attribute'])
            val['product_attribute'] = pr_attr
            delete = val['delete']
            del val['delete']
            if 'id' in val.keys():
                attr_v = AttributeValue.objects.filter(id=val['id'])
                if delete:
                    attr_v.delete()
                else:
                    attr_v.update(**val)
            else:
                if not delete:
                    AttributeValue.objects.create(**val)
        return Response({'status': 200})


class DeleteCategories(generics.UpdateAPIView):
    serializer_class = SetCategorySerializer
    #permission_classes = (AllowAnyGetPermission,)

    def update(self, request, *args, **kwargs):
        for ct in request.data['categories']:
            try:
                category = Category.objects.get(pk=ct['id'])
                category.delete()
            except Product.DoesNotExist:
                pass
        return Response(request.data)


class DeleteTags(generics.UpdateAPIView):
    serializer_class = SetTagSerializer
    #permission_classes = (AllowAnyGetPermission,)

    def update(self, request, *args, **kwargs):
        for ct in request.data['tags']:
            try:
                tag = Tag.objects.get(pk=ct['id'])
                tag.delete()
            except Product.DoesNotExist:
                pass
        return Response(request.data)


class DeleteAttributes(generics.UpdateAPIView):
    serializer_class = SetAttributeSerializer
    #permission_classes = (AllowAnyGetPermission,)

    def update(self, request, *args, **kwargs):
        for at in request.data['attributes']:
            try:
                attr = Attribute.objects.get(pk=at['id'])
                attr.delete()
            except Attribute.DoesNotExist:
                pass
        return Response(request.data)


class DeleteProductAttributes(generics.UpdateAPIView):
    serializer_class = SetProductAttributeSerializer
    # permission_classes = (AllowAnyGetPermission,)

    def update(self, request, *args, **kwargs):
        for at in request.data['product_attributes']:
            try:
                attr = ProductAttribute.objects.get(pk=at['id'])
                attr.delete()
            except ProductAttribute.DoesNotExist:
                pass
        return Response(request.data)


class DeleteProducts(generics.UpdateAPIView):
    serializer_class = SetProductSerializer
    #permission_classes = (AllowAnyGetPermission,)

    def update(self, request, *args, **kwargs):
        for ct in request.data['products']:
            try:
                product = Product.objects.get(pk=ct['id'])
                product.delete()
            except Product.DoesNotExist:
                pass
        return Response(request.data)


class DeleteGroups(generics.UpdateAPIView):
    serializer_class = SetProductSerializer
    #permission_classes = (AllowAnyGetPermission,)

    def update(self, request, *args, **kwargs):
        for ct in request.data['groups']:
            try:
                group = Group.objects.get(pk=ct['id'])
                group.delete()
            except Group.DoesNotExist:
                pass
        return Response(request.data)


class DeleteDiscounts(generics.UpdateAPIView):
    serializer_class = SetDiscountSerializer
    #permission_classes = (AllowAnyGetPermission,)

    def update(self, request, *args, **kwargs):
        for ct in request.data['discounts']:
            try:
                discount = Discount.objects.get(pk=ct['id'])
                discount.delete()
            except Discount.DoesNotExist:
                pass
        return Response(request.data)


class GetVariationsProducts(generics.RetrieveAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductListSerializer
    
    def get(self, request, pk=None):
        pr = self.get_object()
        response = []
        for p_a in pr.product_attribute.all():
            data = {}
            data['attributes'] = p_a.attribute.name
            values = []
            for v in p_a.attribute_value.all():
                values.append(v.value)
            data['values'] = values
            response.append(data)
        return Response(response)

class GetVariations(generics.ListAPIView):
    queryset = AttributeValue.objects.all()
    serializer_class = AttributeValueCRUDSerializer
    
    def get(self, request):
        attr = self.get_queryset()
        data = {}
        for atr in attr:
            if  not atr.product_attribute.attribute.name in data.keys():
                data[atr.product_attribute.attribute.name] = []
            data[atr.product_attribute.attribute.name].append(atr.value)
        return Response(data)