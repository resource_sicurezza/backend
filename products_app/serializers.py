from rest_framework import serializers
from .serializers_admin import (CategorySerializer, TagSerializer, ProductAttributeSerializer,
                                ImageSerializer, ProductDetail)
from products_app.models import Product


class CatalogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'price', 'dolar_price', 'sale_price', 'amount_dsct',
                    'catalog_image', 'catalog_image_secondary')


class DetailProductSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True)
    tag = TagSerializer(many=True)
    product_attribute = ProductAttributeSerializer(many=True)
    image = ImageSerializer(many=True)
    product_details = ProductDetail()

    class Meta:
        model = Product
        fields = ('id', 'name', 'price', 'position', 'in_stock', 'sale_price', 'product_attribute',
                  'image', 'tag', 'category', 'product_details')

