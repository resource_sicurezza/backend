from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.core.files.storage import default_storage
from django.conf import settings
from .models import Product, ProductDetails, AttributeValue, ProductAttribute, Color, Category, Tag, Attribute, \
    ReviewRate, Image, Discount, LookBook, Group
from utils.fieldImage import Base64ImageField


class LookBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = LookBook
        fields = '__all__'


class ColorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Color
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    id = serializers.ModelField(read_only=True, model_field=Category()._meta.get_field('id'))
    name = serializers.CharField(max_length=50, validators=[UniqueValidator(queryset=Category.objects.all())])

    class Meta:
        model = Category
        fields = ('id', 'name', 'description')
        extra_kwargs = {
            'name': {
                'validators': [UniqueValidator(Category.objects.filter(name=''))],
            }
        }


class SetCategorySerializer(serializers.Serializer):
    categories = CategorySerializer(many=True)


class TagSerializer(serializers.ModelSerializer):
    id = serializers.ModelField(model_field=Tag()._meta.get_field('id'), read_only=True)
    name = serializers.CharField(max_length=50, validators=[UniqueValidator(queryset=Tag.objects.all())])

    class Meta:
        model = Tag
        fields = ('id', 'name')
        extra_kwargs = {
            'name': {
                'validators': [UniqueValidator(Tag.objects.filter(name=''))],
            }
        }


class SetTagSerializer(serializers.Serializer):
    categories = TagSerializer(many=True)


class ProductListSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True)
    tag = TagSerializer(many=True)

    class Meta:
        model = Product
        fields = ('id', 'name', 'sku', 'category', 'tag',)


class AttributeValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = AttributeValue
        fields = '__all__'


class AttributeValueCRUDSerializer(serializers.Serializer):
    values = AttributeValueSerializer(many=True)


class AttributeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Attribute
        fields = '__all__'


class SmallProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name')


class SetAttributeSerializer(serializers.Serializer):
    categories = AttributeSerializer(many=True)


class ProductAttributeSerializer(serializers.ModelSerializer):
    attribute_values = AttributeValueSerializer(source='attribute_value', many=True, read_only=True)
    attribute_name = serializers.ReadOnlyField(source='attribute.name')
    product_name = serializers.ReadOnlyField(source='product.name')

    class Meta:
        model = ProductAttribute
        fields = '__all__'
        read_only = ('attribute_name', 'id', 'attribute_values')


class SetProductAttributeSerializer(serializers.Serializer):
    product_attributes = ProductAttributeSerializer(many=True)


class ReviewRateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReviewRate
        fields = ('id', 'review', 'rate')


class ProductDetail(serializers.ModelSerializer):
    review_rate = ReviewRateSerializer(many=True, read_only=True)

    class Meta:
        model = ProductDetails
        fields = ('id', 'description', 'rich_snippet_description', 'keywords', 'average_rate',
                  'rate_counts', 'sum_rates', 'review_rate')
        read_only_fields = ('review_rate',)


class ImageSerializer(serializers.ModelSerializer):
    path = Base64ImageField(
        max_length=None, use_url=True,
    )

    class Meta:
        model = Image
        fields = ('id', 'path', 'position')
        read_only = ('id',)


class DiscountSerializer(serializers.ModelSerializer):
    date_begin = serializers.DateField(format="%d-%m-%Y")
    date_end = serializers.DateField(format="%d-%m-%Y")

    class Meta:
        model = Discount
        fields = '__all__'
        read_only_fields = ('id', )


class SetDiscountSerializer(serializers.Serializer):
    discounts = DiscountSerializer(many=True)


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'


class SetProductSerializer(serializers.Serializer):
    groups = GroupSerializer(many=True)


class ProductSerializer(serializers.ModelSerializer):
    product_details = ProductDetail(required=False, allow_null=True)
    image = ImageSerializer(many=True)

    class Meta:
        model = Product
        fields = ('id', 'sku', 'name', 'price', 'sale_price', 'in_stock', 'product_details',
                  'image', 'category', 'tag', 'dolar_price', 'catalog_image', 'catalog_image_secondary',
                  'stock')
        read_only_fields = ('review_rate', 'product_attribute', 'catalog_image', 'catalog_image_secondary')

    def create(self, validated_data):
        categories = validated_data.pop('category')
        tags = validated_data.pop('tag')
        product_details = validated_data.pop('product_details')
        image = validated_data.pop('image')
        product = Product.objects.create(**validated_data)
        product.category.add(*categories)
        product.tag.add(*tags)
        product.save()
        ProductDetails.objects.create(product=product, **product_details)
        self.save_image(image, product)
        return product

    def update(self, instance, validated_data, pk=None):
        categories = validated_data.pop('category')
        tags = validated_data.pop('tag')
        product_details = validated_data.pop('product_details')
        image = validated_data.pop('image')
        instance.category.clear()
        instance.tag.clear()
        instance.category.add(*categories)
        instance.tag.add(*tags)
        instance.save()
        pr = Product.objects.filter(id=instance.id)
        pr.update(**validated_data)
        if product_details:
            if not ProductDetails.objects.filter(product=instance.pk).exists():
                ProductDetails.objects.create(product=instance, **product_details)
            else:
                ProductDetails.objects.filter(product=instance).update(**product_details)
        self.save_image(image, instance)
        return instance

    def save_image(self, images, instance):
        for image in images:
            dict_images = dict(image)
            image = dict_images['path']
            position = dict_images['position']
            image_path_server = '{0}/products/{1}/'.format(settings.MEDIA_ROOT, instance.id)
            image_path = "{0}catalog_{1}.jpg".format(image_path_server, position)
            if default_storage.exists(image_path):
                default_storage.delete(image_path)
            default_storage.save(image_path, image)
            product = Product.objects.filter(id=instance.id)
            if position == 1:
                product.update(catalog_image='products/' + str(instance.id) + '/catalog_{0}.jpg'.format(position))
            elif position == 2:
                product.update(catalog_image_secondary='products/' + str(instance.id) + '/catalog_{0}.jpg'.format(position))
            img_query = Image.objects.filter(product=instance, position=position)
            if img_query.exists():
                img_query.update(path='products/' + str(instance.id) + '/catalog_{0}.jpg'.format(position))
            else:
                Image.objects.create(product=instance, path='products/' + str(instance.id) + '/catalog_{0}.jpg'.format(position),
                                     position=position)


class SetProductSerializer(serializers.Serializer):
    products = ProductSerializer(many=True)

    class Meta:
        fields = '__all__'
