from django.apps import AppConfig


class SicurezzaAppConfig(AppConfig):
    name = 'products_app'

    def ready(self):
        import products_app.signals
