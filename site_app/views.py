from rest_framework import viewsets, generics
from .serializers import HomeBannerSerializers
from .serializers_admin import StoreSerializer
from .models import HomeBanner, Stores


class HomeBannerCrudView(viewsets.ModelViewSet):
	queryset = HomeBanner.objects.all()
	serializer_class = HomeBannerSerializers


class StoreView(generics.ListAPIView):
	queryset = Stores.objects.all()
	serializer_class = StoreSerializer
