from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter
from .views import HomeBannerCrudView, StoreView
from .views_admin import StoreCrudView, DeleteProducts, BlogCrudView


router = DefaultRouter()
router.register(r'home', HomeBannerCrudView)
router.register(r'store', StoreCrudView)
router.register(r'blog', BlogCrudView)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^list-stores/', StoreView.as_view()),
    url(r'^set-delete-stores/', DeleteProducts.as_view()),
]