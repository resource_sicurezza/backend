from django.db import models


class HomeBanner(models.Model):
    """
    Attributes
    name:
        Nombre de la image
    image:
        Imagen del banner
    """
    name = models.CharField(max_length=100)
    image_side_left = models.ImageField(null=True, blank=True)
    image_side_right = models.ImageField(null=True, blank=True)

    def __str__(self):
        return '{0} - {1}'.format(self.name)


class Coordinates(models.Model):
    lat = models.DecimalField(decimal_places=10, max_digits=100, blank=True, null=True)
    lng = models.DecimalField(decimal_places=10, max_digits=100, blank=True, null=True)


class Stores(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=300, blank=True, null=True)
    reference = models.CharField(max_length=300, blank=True, null=True)
    schedule = models.CharField(max_length=300, blank=True, null=True)
    location = models.OneToOneField(Coordinates, on_delete=models.CASCADE, blank=True, null=True, related_name='store')

    def __str__(self):
        return '{0}'.format(self.name)


class Blog(models.Model):
    title = models.CharField(max_length=100, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    date = models.DateField(auto_now_add=True)
    like = models.IntegerField(null=True, blank=True)
    image = models.ImageField(upload_to="media/", null=True, blank=True)

