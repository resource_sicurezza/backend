# Generated by Django 2.0.2 on 2018-06-20 21:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('site_app', '0007_auto_20180620_1605'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='image',
            field=models.TextField(blank=True, null=True),
        ),
    ]
