# Generated by Django 2.0.2 on 2018-06-20 21:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('site_app', '0006_blog'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='content',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='blog',
            name='like',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
