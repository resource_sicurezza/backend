from rest_framework import serializers
from .models import HomeBanner


class HomeBannerSerializers(serializers.ModelSerializer):
	class Meta:
		model = HomeBanner
		fields = '__all__'