from django.contrib import admin
from .models import (HomeBanner, Coordinates, Stores, Blog)

admin.site.register(HomeBanner)
admin.site.register(Coordinates)
admin.site.register(Stores)
admin.site.register(Blog)
