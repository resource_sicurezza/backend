from rest_framework.response import Response
from rest_framework import viewsets, generics
from .serializers_admin import StoreSerializer, SetStoreSerializer, BlogSerializer
from .models import Stores, Blog


class BlogCrudView(viewsets.ModelViewSet):
    queryset = Blog.objects.all()
    serializer_class = BlogSerializer


class StoreCrudView(viewsets.ModelViewSet):
    queryset = Stores.objects.all()
    serializer_class = StoreSerializer


class DeleteProducts(generics.UpdateAPIView):
    serializer_class = SetStoreSerializer
    #permission_classes = (AllowAnyGetPermission,)

    def update(self, request, *args, **kwargs):
        for ct in request.data['stores']:
            try:
                stores = Stores.objects.get(pk=ct['id'])
                stores.delete()
            except Stores.DoesNotExisWt:
                pass
        return Response(request.data)
