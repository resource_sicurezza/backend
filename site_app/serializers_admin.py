from rest_framework import serializers
from .models import Stores, Coordinates, Blog
from utils.fieldImage import Base64ImageField
from django.conf import settings
from django.core.files.storage import default_storage


class CoordinatesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coordinates
        fields = ('lat', 'lng')


class BlogSerializer(serializers.ModelSerializer):
    image = Base64ImageField(
        max_length=None, use_url=True,
    )

    class Meta:
        model = Blog
        fields = '__all__'
        read_only = ('date', 'like',)

    def create(self, validated_data):
        image = validated_data.pop('image')
        instance = super(BlogSerializer, self).create(validated_data)
        self.save_image(image, instance)
        instance.image = '/blogs/{0}/banner.jpg'.format(instance.id)
        instance.save()
        return instance

    def update(self, instance, validated_data, pk=None):
        image = validated_data.pop('image')
        if image:
            self.save_image(image, instance)
        instance = super(BlogSerializer, self).update(instance, validated_data)
        print(instance.image)
        instance.image = '/blogs/{0}/banner.jpg'.format(instance.id)
        instance.save()
        print(instance.image)
        return instance

    def save_image(self, image, instance):
        image_path_server = '{0}/blogs/{1}/'.format(settings.MEDIA_ROOT, instance.id)
        image_path = "{0}banner.jpg".format(image_path_server)
        if default_storage.exists(image_path):
            default_storage.delete(image_path)
        default_storage.save(image_path, image)


class StoreSerializer(serializers.ModelSerializer):
    location = CoordinatesSerializer()

    class Meta:
        model = Stores
        fields = '__all__'

    def create(self, validated_data):
        location = validated_data.pop('location')
        instance = super(StoreSerializer, self).create(validated_data)
        coordinates = Coordinates.objects.create(**location)
        instance.location = coordinates
        instance.save()
        return instance

    def update(self, instance, validated_data, pk=None):
        location = validated_data.pop('location')
        if location:
            coordenates = Coordinates.objects.filter(store=instance)
            coordenates.update(**location)
        instance = super(StoreSerializer, self).update(instance, validated_data)
        return instance


class SetStoreSerializer(serializers.Serializer):
    stores = StoreSerializer(many=True)

    class Meta:
        fields = '__all__'
