from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from rest_framework.documentation import include_docs_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('ecommerce/product/', include('products_app.urls')),
    path('ecommerce/client/', include('client_app.urls')),
    path('ecommerce/site/', include('site_app.urls')),
    path('ecommerce/sale/', include('sale_app.urls')),
    path('docs/', include_docs_urls(title='Ecommerce')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
