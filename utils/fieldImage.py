from rest_framework import serializers


class Base64ImageField(serializers.ImageField):

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        if data:
            data_64 = data['value']
            if 'data:' in data_64 and ';base64,' in data_64:
                header, data_64 = data_64.split(';base64,')
            try:
                decoded_file = base64.b64decode(data_64)
            except TypeError:
                self.fail('invalid_image')
            # Get the file name extension:
            file_extension = self.get_file_extension(data['filename'], decoded_file)
            complete_file_name = "%s.%s" % (data['filename'], file_extension, )
            data = ContentFile(decoded_file, name=complete_file_name)
        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension