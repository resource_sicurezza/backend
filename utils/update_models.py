class TransformObjectToModel:

    def return_objects(self, objects, model):
        array_obj = []
        for obj in objects:
            items = list(obj.items())
            if items[0][0] == 'id':
                array_obj.append(model.objects.get(pk=items[0][1]))
        return array_obj
